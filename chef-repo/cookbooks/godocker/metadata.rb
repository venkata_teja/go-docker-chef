name             'godocker'
maintainer       'IRISA'
maintainer_email 'olivier.sallou@irisa.fr'
license          'Apache 2.0'
description      'Installs/Configures godocker'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'docker', '~> 2.0'
depends 'nfs', '~> 2.2.6'
