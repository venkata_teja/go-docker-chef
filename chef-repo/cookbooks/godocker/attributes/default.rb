default['godocker'] = {
    'master' => {
        'ip' => nil
    },
    'user' => {
        'name' => 'godocker',
        'password' => 'godocker',
        'uid' => 1001,
        'gid' => 1001
    }
}
