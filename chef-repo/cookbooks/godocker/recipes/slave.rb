#
# Cookbook Name:: godocker
# Recipe:: master
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

docker_image 'swarm' do
  action :pull
  host 'tcp://'+node['ipaddress']+':2375'
end


mount  "/opt/godshared" do
  fstype 'nfs'
  action [:mount, :enable]
  device node['godocker']['master']['ip']+':/opt/godshared'
  mount_point "/opt/godshared"
  options 'rw'
end


docker_container 'god-swarm' do
    repo 'swarm'
    name 'god-swarm'
    tag 'latest'
    host 'tcp://'+node['ipaddress']+':2375'
    port "2376:2375"
    command 'join --advertise '+node['ipaddress']+':2375 zk://'+node['godocker']['master']['ip']+'/godocker'
    action :run
    remove_volumes true
end
