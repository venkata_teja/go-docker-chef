#
# Cookbook Name:: godocker
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

group 'godocker' do
  action :create
  append true
  gid node['godocker']['user']['gid']
end

directory '/opt/godshared/home' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  ignore_failure true
  recursive true
end


user 'godocker' do
  username node['godocker']['user']['name']
  comment 'GoDocker base user'
  gid node['godocker']['user']['gid']
  home '/opt/godshared/home/'+node['godocker']['user']['name']
  manage_home false
  shell '/bin/bash'
  # password: godockerdemo
  password '$1$8SW.0rmU$X64XsU1gMHtKn8VLIZ4HT.'
end

docker_service 'default' do
  host [ "tcp://#{node['ipaddress']}:2375" ]
  action [:create, :start]
end

package 'godocker_packages' do
  case node[:platform]
  when 'redhat', 'centos'
    package_name ['python', 'python-setuptools', 'nfs-utils', 'nfs-utils-lib',
'git']
  when 'ubuntu', 'debian'
    package_name ['python', 'python-setuptools', 'nfs-kernel-server', 'git']
  end
end
